# <The project name>
<A description outlining the purpose of this project>

## SAMPLES:
```
  /****************/
 /* Hello Blinky */
/****************/

#include "@PROJECT_NAME.h"

void setup (void) {
	pinMode(LED_BUILTIN, OUTPUT);
}

void loop (void) {
	digitalWrite(LED_BUILTIN, HIGH);
	delay(500);
	digitalWrite(LED_BUILTIN, LOW);
	delay(500);
}
```

## TODO:
- <Create a project and fill in the bracketed information>
- <Replace the SAMPLE code with an actual sample>

## ATTRIBUTION:
The makefiles used for compiling the Google Unit Test where taken from Google.

## LICENSE:
The MIT License (MIT). Refer to the LICENSE file for more details.
